package br.com.qualitymetrics.palindromo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PalindromoServiceTest {

	private PalindromoService palindromoService = new PalindromoService();
	
	@Test
	public void testDeveSerPalindromo() {
		
		assertEquals(true, palindromoService.isPalindromo("Rotator"));
		assertEquals(true, palindromoService.isPalindromo("bob"));
		assertEquals(true, palindromoService.isPalindromo("madam"));
		assertEquals(true, palindromoService.isPalindromo("mAlAyAlam"));
		assertEquals(true, palindromoService.isPalindromo("1"));
		assertEquals(true, palindromoService.isPalindromo("Able was I, ere I saw Elba"));
		assertEquals(true, palindromoService.isPalindromo("Madam I’m Adam"));
		assertEquals(true, palindromoService.isPalindromo("Step on no pets."));
		assertEquals(true, palindromoService.isPalindromo("Top spot!"));
		assertEquals(true, palindromoService.isPalindromo("02/02/2020"));
		assertEquals(true, palindromoService.isPalindromo("Socorram-me subi no ônibus em marrocos"));
		
	}
	
	@Test
	public void testNaoDeverSerPalindromo() {
	
		assertEquals(false, palindromoService.isPalindromo(null));
		assertEquals(false, palindromoService.isPalindromo("xyz"));
		assertEquals(false, palindromoService.isPalindromo("elephant"));
		assertEquals(false, palindromoService.isPalindromo("Country"));
		assertEquals(false, palindromoService.isPalindromo("Top . post!"));
		assertEquals(false, palindromoService.isPalindromo("Wonderful-fool"));
		assertEquals(false, palindromoService.isPalindromo("Wild imagination!"));
	}

}
