package br.com.qualitymetrics.palindromo;

import java.text.Normalizer;
import java.util.Objects;

public class PalindromoService {

	public boolean isPalindromo(String valor) {
		
		if (Objects.isNull(valor)) {
			return false;
		}
		
		valor = valor.toUpperCase();
		valor = valor.replace(" ", "");
		valor =  valor.replaceAll("[^\\p{L}\\p{Z}]","");
		valor =   Normalizer.normalize(valor, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		
		StringBuilder reversoSB = new StringBuilder(valor);
		String reverso = reversoSB.reverse().toString();
		
		return valor.equals(reverso);
	}

}
